// ignore_for_file: avoid_print

main() {
  //Create an array to store all the winning apps of the MTN Business App of the Year Awards since 2012
  var winningAppArray = [
    'FNB - 2012',
    'SnapScan - 2013',
    'LIVE Inspect - 2014',
    'WumDrop - 2015',
    'Domestly - 2016',
    'Shyft - 2017',
    'Khula ecosystem - 2018',
    'Naked Insurance - 2019',
    'EasyEquities - 2020',
    'Ambani - 2021'
  ];

  //Sort and print the apps by name
  winningAppArray.sort();
  print(winningAppArray.toString());

  for (var i = 0; i < winningAppArray.length; i++) {
    //Print the winning app of 2017 from the sorted array
    if (winningAppArray[i].contains('2017')) {
      print('Winning app of 2017: ${winningAppArray[i]}');
    }
    //Print the winning app of 2018 from the sorted array
    if (winningAppArray[i].contains('2018')) {
      print('Winning App of 2018: ${winningAppArray[i]}');
    }
  }

  //Print total number of apps from the array
  print('Total number of Apps: ${winningAppArray.length}');
}
