// ignore_for_file: avoid_print

class MTNAPPS {
  String appName = 'Ambani';
  String appSector = 'Education';
  String appDeveloper = 'Mukundi Lambani';
  String appYear = '2021';

  //Create a function inside the class, transform the app name to all capital letters and then print the output.
  void capitalizeName() {
    String capitalizedName = appName.toUpperCase();
    print('Capitalize App Name is: $capitalizedName');
  }
}

main() {
  MTNAPPS mtnapps = MTNAPPS();
  print(
      'App Name: ${mtnapps.appName}, Sector: ${mtnapps.appSector}, Developer: ${mtnapps.appDeveloper}, Year: ${mtnapps.appYear}');
  mtnapps.capitalizeName();
}
