// importing dart:io file
import 'dart:io';

void main() {
  //Your name
  print('Enter your name: ');
  // Reading your name
  String? name = stdin.readLineSync();
  //Reading your favorite app
  print('Enter your favorite app: ');
  String? favouriteApp = stdin.readLineSync();
  //Readng your city
  print('Enter your city');
  String? city = stdin.readLineSync();

  print(
      'Your name is: $name, your favorite app is $favouriteApp, and your city is $city.');
}
